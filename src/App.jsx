import { useQuery, useMutation, useQueryClient } from "@tanstack/react-query";
import './App.css';

const url = 'https://jsonplaceholder.typicode.com/todos';
const url2 = 'https://jsonplaceholder.typicode.com/posts';


function App() {

  const queryClient = useQueryClient();

  const query = useQuery({
    queryKey: ['posts'],
    queryFn: () => fetch(url2).then((resp) => resp.json()),
    gcTime: 6000,
    staleTime: 60_000,
    // refetchInterval: 5_000
    refetchOnWindowFocus: false,
    retry: 5,
  });

  const mutation = useMutation({
    mutationFn: (newPost) => fetch(url2, {
      method: "POST",
      body: JSON.stringify(newPost),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    }).then(resp => resp.json()),

    onSuccess: (newPost) => {
      console.log('data mutation request send successfully...', newPost);
      // queryClient.invalidateQueries({ queryKey: ["posts"] });

      queryClient.setQueryData(['posts'], (oldPosts) => {
        return [...oldPosts, newPost];
      });
    }
  });

  if (query.error || mutation.isError) return <div> There was an error ! </div>;
  if (query.isLoading) return <div> Data is loading... </div>;

  return (
    <div>
      {
        mutation.isPending && (<p>
          adding post...
        </p>)
      }
      <button
        onClick={() => mutation.mutate({
          "userId": 97,
          "title": "tanstack query is awesome",
          "body": "TanStack Query (FKA React Query) is often described as the missing data-fetching library for web applications, but in more technical terms, it makes fetching, caching, synchronizing and updating server state in your web applications a breeze."
        })}
      >
        Add Post
      </button>
      {
        query.data?.map((todo, index) => (
          <div key={index}>
            <p> ID: {todo.id} || TITLE: {todo.title} || BODY: {todo.body} </p>
          </div>
        ))
      }
    </div>
  )
}


export default App;
